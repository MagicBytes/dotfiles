#!/bin/bash
RANDOMNUMBER=`od -vAn -N4 -tu4 < /dev/urandom`
Date=`date "+%b_%d_%Y"`
Timestamp=`date "+%s"`
xsnap -nogui -file "$HOME/screenshot-$Date-$Timestamp-$RANDOMNUMBER.png"
