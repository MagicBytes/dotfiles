export ZSH=/home/pascal/.oh-my-zsh
export QT_SELECT=5
export QT_QPA_PLATFORMTHEME="qt5ct"
export ANDROID_HOME=/opt/android-sdk
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk
export LD_LIBRARY_PATH=/opt/oracle/instantclient:$LD_LIBRARY_PATH
export OCI_LIB_DIR=/opt/oracle/instantclient
export OCI_INC_DIR=/opt/oracle/instantclient/sdk/include

plugins=(git zsh-history-substring-search zsh-autosuggestions)

ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=60"

#------------------------------------------////
# Colors:
#------------------------------------------////
black='\e[0;30m'
blue='\e[0;34m'
green='\e[0;32m'
cyan='\e[0;36m'
red='\e[0;31m'
purple='\e[0;35m'
brown='\e[0;33m'
lightgray='\e[0;37m'
darkgray='\e[1;30m'
lightblue='\e[1;34m'
lightgreen='\e[1;32m'
lightcyan='\e[1;36m'
lightred='\e[1;31m'
lightpurple='\e[1;35m'
yellow='\e[1;33m'
white='\e[1;37m'
nc='\e[0m'

#Color table
fg_black=%{$'\e[0;30m'%}
fg_red=%{$'\e[0;31m'%}
fg_green=%{$'\e[0;32m'%}
fg_brown=%{$'\e[0;33m'%}
fg_blue=%{$'\e[0;34m'%}
fg_purple=%{$'\e[0;35m'%}
fg_cyan=%{$'\e[0;36m'%}
fg_lgray=%{$'\e[0;37m'%}
fg_dgray=%{$'\e[1;30m'%}
fg_lred=%{$'\e[1;31m'%}
fg_lgreen=%{$'\e[1;32m'%}
fg_yellow=%{$'\e[1;33m'%}
fg_lblue=%{$'\e[1;34m'%}
fg_pink=%{$'\e[1;35m'%}
fg_lcyan=%{$'\e[1;36m'%}
fg_white=%{$'\e[1;37m'%}
#Text Background Colors
bg_red=%{$'\e[0;41m'%}
bg_green=%{$'\e[0;42m'%}
bg_brown=%{$'\e[0;43m'%}
bg_blue=%{$'\e[0;44m'%}
bg_purple=%{$'\e[0;45m'%}
bg_cyan=%{$'\e[0;46m'%}
bg_gray=%{$'\e[0;47m'%}
#Attributes
at_normal=%{$'\e[0m'%}
at_bold=%{$'\e[1m'%}
at_italics=%{$'\e[3m'%}
at_underl=%{$'\e[4m'%}
at_blink=%{$'\e[5m'%}
at_outline=%{$'\e[6m'%}
at_reverse=%{$'\e[7m'%}
at_nondisp=%{$'\e[8m'%}
at_strike=%{$'\e[9m'%}
at_boldoff=%{$'\e[22m'%}
at_italicsoff=%{$'\e[23m'%}
at_underloff=%{$'\e[24m'%}
at_blinkoff=%{$'\e[25m'%}
at_reverseoff=%{$'\e[27m'%}
at_strikeoff=%{$'\e[29m'%}
 
# env  
export EDITOR=vim
export PAGER=less
export PATH=${PATH}:/usr/lib/x86_64-linux-gnu
fpath=(path/to/zsh-completions $fpath)
export LANG="de_DE.UTF-8"
export LC_CTYPE="de_DE.UTF-8"
setopt NO_HUP
setopt IGNORE_EOF
setopt NO_CASE_GLOB
setopt NUMERIC_GLOB_SORT
setopt EXTENDED_GLOB
setopt list_ambiguous

source $ZSH/oh-my-zsh.sh

if [[ -x `which irb1.8` ]]; then
  alias irb='irb1.8 --readline -r irb/completion'
else
  alias irb='irb --readline -r irb/completion'
fi

alias cpv="rsync -poghb --backup-dir=/tmp/rsync -e /dev/null --progress --"

# prompt
PROMPT="
#${fg_lgray}%n@${at_bold}%m${at_boldoff}${fg_dgray}[${fg_white}%~${fg_dgray}]
#[${fg_white}%T${fg_dgray}]:${at_normal}"

#Prompt
#autoload promptinit
#promptinit
#colors
#PS1=$'%{\e[1;30m%}[%{\e[0m%} %{\e[1;35m%}%~%{\e[0m%}%{\e[1;30m%}]%{\e[0m%}%{\e[0;34m%}$%{\e[0m%} '                                
#
#based on Barrucadu (prompt):https://bbs.archlinux.org/viewtopic.php?pid=831695#p831695
# 

# prompt
function prompt_git_dirty() {
    gitstat=$(git status 2>/dev/null | grep '\(# Untracked\|# Changes\|# Changed but not updated:\)')
    
    if [[ $(echo ${gitstat} | grep -c "^# Changes to be committed:$") > 0 ]]; then
        echo -n $fg_yellow
    elif [[ $(echo ${gitstat} | grep -c "^\(# Untracked files:\|# Changed but not updated:\)$") > 0 ]]; then
        echo -n $fg_lred
    else
        echo -n $fg_pink
    fi
}

function prompt_current_branch() {
    ref=$(git symbolic-ref HEAD 2> /dev/null) || return 1
    echo ${ref#refs/heads/}
}

function prompt_hostname()
{
    case "`hostname`" in
        "Arch")
            echo -n "${fg_yellow}Arch${at_normal}";;    
    esac
}

function precmd() # Uses: setting user/root PROMPT1 variable and rehashing commands list
{
    # Last command status
    cmdstatus=$?
    sadface=`[ "$cmdstatus" != "0" ] && echo "${fg_red}:(${at_normal} "`

    # Colours
    usercolour=`[ $UID != 0 ]   && echo $fg_blue      || echo $fg_red`
    usercolour2=`[ $UID != 0 ]  && echo $fg_white || echo $fg_red`
    dircolour=`[ -w "\`pwd\`" ] && echo $fg_lgray       || echo $fg_red`

    # Git branch
    git="[branch: `prompt_git_dirty``prompt_current_branch`${fg_blue}]"

export PROMPT="
${usercolour}┌─[${dircolour}%n${PR_NO_COLOR}«»`prompt_hostname`${usercolour}]─────────────────────────${dircolour}[%~]${at_normal} `prompt_current_branch &>/dev/null && echo -n $git`
${usercolour}└─${sadface}${usercolour}${dircolour}(%T)(%l)${usercolour} coruscant ─╼ ${at_normal}"                          
}                                        
#zmodload zsh/pcre


#{{{ Completion Stuff
zmodload -i zsh/complist
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
bindkey -M viins '\C-i' complete-word
# Faster! (?)
zstyle ':completion::complete:*' use-cache 1
# case insensitive completion
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}' \
     'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' verbose yes
zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
zstyle ':completion:*' group-name ''
# generate descriptions with magic.
zstyle ':completion:*' auto-description 'specify: %d'
# Don't prompt for a huge list, page it!
zstyle ':completion:*:default' list-prompt '%S%M matches%s'
# Don't prompt for a huge list, menu it!
zstyle ':completion:*:default' menu 'select=0'
# Have the newer files last so I see them first
zstyle ':completion:*' file-sort modification reverse
# color code completion!!!!  Wohoo!
zstyle ':completion:*' list-colors "=(#b) #([0-9]#)*=36=31"
unsetopt LIST_AMBIGUOUS
setopt  COMPLETE_IN_WORD
# Separate man page sections.  Neat.
zstyle ':completion:*:manuals' separate-sections true
# Egomaniac!
zstyle ':completion:*' list-separator 'fREW'
# complete with a menu for xwindow ids
zstyle ':completion:*:windows' menu on=0
zstyle ':completion:*:expand:*' tag-order all-expansions
# more errors allowed for large words and fewer for small words
zstyle ':completion:*:approximate:*' max-errors 'reply=(  $((  ($#PREFIX+$#SUFFIX)/3  ))  )'
# Errors format
zstyle ':completion:*:corrections' format '%B%d (errors %e)%b'
# Don't complete stuff already on the line
zstyle ':completion::*:(rm|vi):*' ignore-line true
# Don't complete directory we are already in (../here)
zstyle ':completion:*' ignore-parents parent pwd
zstyle ':completion::approximate*:*' prefix-needed false

#}}}


 

## FUNCTIONS

cdl() {builtin cd $@; ls }

# Easy extract
extract () {
  if [ -f $1 ] ; then
      case $1 in
          *.tar.bz2)   tar xvjf $1    ;;
          *.tar.gz)    tar xvzf $1    ;;
          *.bz2)       bunzip2 $1     ;;
          *.rar)       rar x $1       ;;
          *.gz)        gunzip $1      ;;
          *.tar)       tar xvf $1     ;;
          *.tbz2)      tar xvjf $1    ;;
          *.tgz)       tar xvzf $1    ;;
          *.zip)       unzip $1       ;;
          *.Z)         uncompress $1  ;;
          *.7z)        7z x $1        ;;
          *)           echo "don't know how to extract '$1'..." ;;
      esac
  else
      echo "'$1' is not a valid file!"
  fi
}

# Makes directory then moves into it
function mkcdr {
    mkdir -p -v $1
    cd $1
}

# Creates an archive from given directory
mktar() { tar cvf  "${1%%/}.tar"     "${1%%/}/"; }
mktgz() { tar cvzf "${1%%/}.tar.gz"  "${1%%/}/"; }
mktbz() { tar cvjf "${1%%/}.tar.bz2" "${1%%/}/"; }

upinfo ()
{
echo -ne "\t ";uptime | awk /'up/ {print $3,$4,$5,$6,$7,$8,$9,$10}'
}

# list files only
lf () { ls -1p $@ | grep -v '\/$' }


# define words with wordnet
ddefine () { curl dict://dict.org/d:${1}:wn; }


## Aliases

alias ls='ls --color=auto' # color is useful.
alias l='ls -lFh'  
alias la='ls -lAFh'   
alias lr='ls -tRFh'   
alias lt='ls -ltFh'
alias ll='ls -al'
alias cp='cp -i'
alias mv='mv -i'
alias mkdir='mkdir -p -v'
alias df='df -h'
alias du='du -h -c'
alias reload='source ~/.zshrc'
alias biggest='BLOCKSIZE=1048576; du -x | sort -nr | head -10'
alias rmdir='"rm" -rfv'
alias rm='rm -v'
alias trashman='"rm" -rf ~/.local/share/Trash/files/*'
alias scrott='"scrot" -d 7 -c' 
alias zshrc="$EDITOR ~/.zshrc"
alias mountiso="sudo mount -t iso9660 -o loop"
alias blank="sleep 1 && xset dpms force off"
alias lesslast="less !:*"
alias mine="sudo chown pascal:pascal"
alias apacman="~/ansipacman.sh"
alias atank="~/ansitanks.sh"
alias invaders="~/colors.sh"
alias -g G="| grep"
alias -g L="| less"
alias reboot='sudo reboot'

## changing dirs with just dots. 
alias .='cd ../'
alias ..='cd ../../'
alias ...='cd ../../../'
alias ....='cd ../../../../'

## these are the dirs i use most commonly.
alias home='cd ~/'
alias documents='cd ~/Dokumente'
alias downloads='cd ~/Downloads'
alias books='cd ~/eBooks'
alias pictures='cd ~/Bilder'
alias torrents='cd ~/Torrents'
alias videos='cd ~/Videos'
alias wallpaper='cd ~/Bilder/Wallpaper'
alias music='cd ~/Musik'

## MORE aliases that i use a lot.
alias ports='netstat -nape --inet'
alias ping='ping -c 4'
alias ns='netstat -alnp --protocol=inet'
alias top="sudo htop d 1"
alias    ps="ps aux"
alias today='date "+%d %h %y"'
alias now='date "+%A, %d %B, %G%t%H:%M %Z(%z)"'
alias msgs='tail -f /var/log/messages'
alias grep="egrep --color=auto"  # color grep output
alias cls="clear;fortune"     # another quickie
alias k=kill                     # QUICKKEY - kill proces
alias e=$EDITOR                  # QUICKKEY - edit quickly
alias m=more                     # QUICKKEY - yeah
alias q=exit                     # QUICKKEY - like vim
alias size='du -sh'

## Package Manager - Uncomment for your distro
#  Distributions Included: Debian and Debian-based Distributions like Ubuntu and Mint
#                          Fedora, and other Yum-based Distros like OpenSUSE and Fuduntu
#                          Sayabon, Gentoo, Archlinux.

## Debian/Debian-based.  Ubuntu, Mint, Crunchbang, etc

#alias ainstall='sudo aptitude install'
#alias aremove='sudo aptitude remove'
#alias system_update='sudo aptitude safe-upgrade'
#alias afind='aptitude search'
#alias affind='sudo apt-file search'
#alias afupdate='sudo apt-file update'
#alias aupdate='sudo aptitude update'
#alias addppa='sudo add-apt-repository'
#alias orphand='sudo deborphan | xargs sudo apt-get -y remove --purge'
#alias cleanup='sudo apt-get autoclean && sudo apt-get autoremove && 
#sudo apt-get clean && sudo apt-get remove && orphand'
#alias updatedb='sudo updatedb'
#alias lessswap="sudo sysctl vm.swappiness=10"
#alias moreswap="sudo sysctl vm.swappiness=100"

## Fedora/Fuduntu/etc

#alias yinstall='sudo yum install'
#alias yremove='sudo yum remove'
#alias system_update='sudo yum update'
#alias yfind='sudo yum search'
#alias yginstall='sudo yum groupinstall'

## Sabayon

#alias einstall='sudo equo install'
#alias efind='equo search'
#alias eremove='sudo equo remove'
#alias eupdate='sudo equo update'
#alias eupgrade='sudo equo upgrade --ask'

## Gentoo

#alias einstall='sudo emerge -a'
#alias eremove='sudo emerge -ac'
#alias system_update='sudo emerge -aev world'
#alias efind='sudo emerge -S'
#alias eupdate='sudo emerge --sync'
#alias depclean='sudo emerge --depclean'

## Archlinux

alias aurinst='yaourt -S'
alias instpkg='sudo pacman -U'
alias install='sudo pacman -S'
alias remove='sudo pacman -R'
alias system_update='sudo pacman -Syu'
alias aurfind='yaourt -Ss'
alias pfind='sudo pacman -Ss'
alias pinfo='sudo pacman -Si'
alias installedby='sudo pacman -Ql'
alias whatfrom='sudo pacman -Qo'
alias showorphan='sudo pacman -Qdt'
alias removeuseless='sudo pacman -Rsnc $(pacman -Qdqt)'
alias pacupgrade='sh ~/.mirrorfix'
alias giveswhat='sudo pkgfile --list'
## Misc
alias edit='vim'

## Keybinding Fixes
# Stops the damn tilde

bindkey "\e[1~" beginning-of-line # Home
bindkey "\e[4~" end-of-line # End
bindkey "\e[5~" beginning-of-history # PageUp
bindkey "\e[6~" end-of-history # PageDown
bindkey "\e[2~" quoted-insert # Ins
bindkey "\e[3~" delete-char # Del
bindkey "\e[5C" forward-word
bindkey "\eOc" emacs-forward-word
bindkey "\e[5D" backward-word
bindkey "\eOd" emacs-backward-word
bindkey "\e\e[C" forward-word
bindkey "\e\e[D" backward-word
bindkey "\e[Z" reverse-menu-complete # Shift+Tab
# for rxvt
#bindkey "\e[7~" beginning-of-line # Home
#bindkey "\e[8~" end-of-line # End
# for non RH/Debian xterm, can't hurt for RH/Debian xterm
bindkey "\eOH" beginning-of-line
bindkey "\eOF" end-of-line
# for freebsd console
#bindkey "\e[H" beginning-of-line
#bindkey "\e[F" end-of-line
# for guake
#bindkey "\eOF" end-of-line
#bindkey "\eOH" beginning-of-line
#bindkey "^[[1;5D" backward-word
#bindkey "^[[1;5C" forward-word
#bindkey "\e[3~" delete-char # Del
# Incremental search is elite!
#bindkey -M vicmd "/" history-incremental-search-backward
#bindkey -M vicmd "?" history-incremental-search-forward

# Search based on what you typed in already
#bindkey -M vicmd "//" history-beginning-search-backward
#bindkey -M vicmd "??" history-beginning-search-forward

# oh wow!  This is killer...  try it!
#bindkey -M vicmd "q" push-line
#bindkey -M vicmd '!' edit-command-output

# it's like, space AND completion.  Gnarlbot.
#bindkey -M viins ' ' magic-space

#------------------------------------------////
# System Information:
#------------------------------------------////
clear
echo -e "${white}";toilet -f future --metal "Welcome, " $USER@$HOST;
echo -ne "${blue}Today is:\t\t${darkgray}" `date`; echo ""
echo -e "${blue}Kernel Information: \t${darkgray}" `uname -smr`
echo -ne "${purple}";upinfo;echo ""
echo -e "${blue}"; cal -3

source /home/pascal/.oh-my-zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

PATH="/home/pascal/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/home/pascal/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/pascal/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/pascal/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/pascal/perl5"; export PERL_MM_OPT;
export PATH=$PATH:~/.gem/ruby/2.4.0/bin
